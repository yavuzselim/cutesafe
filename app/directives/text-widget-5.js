/**
 * @author Batch Themes Ltd.
 * @example <text-widget-5 title="{{title}}" amount="1234" icon="{{icon}}" increment="true" duration="1000" icon-color="white" circle-color="success"></text-widget-5>
 */
(function() {
    'use strict';
    app.directive('textWidget5', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-5.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.icon = attrs.icon;
                    scope.increment = attrs.increment || false;
                    scope.duration = attrs.duration || 1;
                    scope.iconColor = attrs.iconColor;
                    scope.circleColor = attrs.circleColor;
                }
            };
        });
})();
