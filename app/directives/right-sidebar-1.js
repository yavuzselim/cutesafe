/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
   app.directive('rightSidebar1', function(SampleData, Functions) {
            return {
                scope: {},
                templateUrl: 'views/right-sidebar/right-sidebar-1.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.timeline = SampleData.getTimeline();
                    scope.githubActivity = SampleData.getGithubActivity();
                    scope.onlineUsers = SampleData.getUsers();
                    scope.offlineUsers = Functions.reverseArray(SampleData.getUsers());
                    scope.salesStream = SampleData.getSalesStream();
                }
            };
        });
})();
