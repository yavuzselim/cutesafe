/**
 * @author Batch Themes Ltd.
 * @example <text-widget-1 title="{{title}}" amount="5" icon="{{icon}}" increment="true" duration="1000" icon-color="white" circle-color="danger"></text-widget-1>
 */
(function() {
    'use strict';
   app.directive('textWidget1', function() {
            return {
                scope: {fiyat:'=fiyat'},
                templateUrl: 'views/text-widgets/text-widget-1.html',
                restrict: 'E',

                link: function(scope, element, attrs) {


                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.market = attrs.market;
                    scope.cointype = attrs.cointype;
                    scope.coinname = attrs.coinname;
                    scope.pluralname = attrs.pluralname;
                    scope.icon = attrs.icon;
                    scope.increment = attrs.increment || false;
                    scope.duration = attrs.duration || 1;
                    scope.iconColor = attrs.iconColor;
                    scope.circleColor = attrs.circleColor;

                    
                }
            };
        });
})();
