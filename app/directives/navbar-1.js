/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.directive('navbar1', function($localStorage, Translations, Navigation, SampleData, $filter, Colors, $location) {
            return {
                scope: {},
                templateUrl: 'views/navbar/navbar-1.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.items = Navigation.get();
                    var user = SampleData.getUsers();
                    scope.profileImage = user[0].img;
                    scope.profileTitle = user[0].name;
                    scope.profileSubtitle = $filter('username')(user[0].name) + '@gmail.com';
                    scope.toggleSearch = function() {
                        var navbarDrawer = $('.navbar-drawer');
                        navbarDrawer.toggleClass('active');
                    };
                    scope.toggleSearchInput = function() {
                        $('.navbar-search').toggleClass('navbar-search-hidden');
                    };
                    var direction = $('body').attr('data-direction');
                    var directionText = 'RTL';
                    if (direction === 'rtl') {
                        directionText = 'LTR';
                    }
                    $('.direction').text(directionText);
                    scope.toggleDirection = function() {
                        var direction = $('body').attr('data-direction');
                        direction = direction == 'rtl' ? 'ltr' : 'rtl';
                        $('body').attr('data-direction', direction);
                        $localStorage.config.direction = direction;
                        var directionText = 'RTL';
                        if (direction === 'rtl') {
                            directionText = 'LTR';
                        }
                        $('.direction').text(directionText);
                    };
                    var language = $localStorage.config.language || 'en';
                    scope.defaultFlag = language;
                    scope.texts = Translations.getTranslations(language);
                    scope.changeLanguage = function(lang) {
                        scope.defaultFlag = lang;
                        $localStorage.config.language = lang;
                        scope.texts = Translations.getTranslations(lang);
                        scope.flags = [{
                            'key': 'en',
                            'title': scope.texts.english,
                            'flag': 'flag-icon flag-icon-gb'
                        }, {
                            'key': 'de',
                            'title': scope.texts.german,
                            'flag': 'flag-icon flag-icon-de'
                        }, {
                            'key': 'es',
                            'title': scope.texts.spanish,
                            'flag': 'flag-icon flag-icon-es'
                        }, {
                            'key': 'fr',
                            'title': scope.texts.french,
                            'flag': 'flag-icon flag-icon-fr'
                        }, ];
                    };
                    scope.flags = [{
                        'key': 'en',
                        'title': scope.texts.english,
                        'flag': 'flag-icon flag-icon-gb'
                    }, {
                        'key': 'de',
                        'title': scope.texts.german,
                        'flag': 'flag-icon flag-icon-de'
                    }, {
                        'key': 'es',
                        'title': scope.texts.spanish,
                        'flag': 'flag-icon flag-icon-es'
                    }, {
                        'key': 'fr',
                        'title': scope.texts.french,
                        'flag': 'flag-icon flag-icon-fr'
                    }, ];
                    scope.toggleRightSidebar = function() {
                        
                        $('.right-sidebar-outer').toggleClass('show-from-right');
                    };
                    scope.toggleLayout = function() {
                        var layout = $localStorage.config.layout;
                        if (layout === 'default-sidebar') {
                            $('body').attr('data-layout', 'collapsed-sidebar');
                            $localStorage.config.layout = 'collapsed-sidebar';
                        } else if (layout === 'collapsed-sidebar') {
                            $('body').attr('data-layout', 'default-sidebar');
                            $localStorage.config.layout = 'default-sidebar';
                        } else {
                            $('body').toggleClass('layout-collapsed');
                        }
                    };
                    scope.toggleFullscreenMode = function() {
                        $(document).fullScreen(true);
                    };
                    var palettes = Colors.getPalettes();
                    var themes = [];
                    for (var i in palettes) {
                        themes.push({
                            name: i,
                            backgroundColor: palettes[i].backgroundColor,
                            leftSidebarBackgroundColor: palettes[i].leftSidebarBackgroundColor,
                            highlightColor: palettes[i].highlightColor
                        });
                    }
                    scope.themes = angular.copy(themes);
                    scope.toggleThemeSelector = function() {
                        $('.theme-selector').toggleClass('show-theme-selector');
                    };
                    scope.setPalette = function(palette) {
                        $localStorage.config.theme = palette;
                        $localStorage.config.palette = Colors.getPalette(palette),
                            $location.path('/#/' + palette);
                        $('body').attr('data-palette', palette);
                    };
                }
            };
        });
})();
