/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.directive('fullsizeBackgroundImage1', function() {
            return {
                scope: {},
                templateUrl: 'views/fullsize-background-image/fullsize-background-image-1.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                }
            };
        });
})();
