/**
 * @author Batch Themes Ltd.
 * @example <text-widget-7 title="{{title}}" amount="1234" align="left" bg-color="white" text-color="success"></text-widget-7>
 */
(function() {
    'use strict';
    app.directive('textWidget7s', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-7s.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.align = attrs.align;
                    scope.bgColor = attrs.bgColor;
                    scope.textColor = attrs.textColor;
                }
            };
        });
})();
