/**
 * @author Batch Themes Ltd.
 * @example <finance-widget-1 items="{{items}}" title="{{title}}" amount="5"></finance-widget-1>
 */
(function() {
    'use strict';
   app.directive('financeWidget1', function() {
            return {
                scope: {},
                templateUrl: 'views/finance-widgets/finance-widget-1.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.items = angular.fromJson(attrs.items);
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                }
            };
        });
})();
