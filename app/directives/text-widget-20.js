/**
 * @author Batch Themes Ltd.
 * @example <text-widget-20 text-color="white" text="fa fa-heart-o" amount-color="white" amount="20"></text-widget-20>
 */
(function() {
    'use strict';
   app.directive('textWidget20', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-20.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.textColor = attrs.textColor;
                    scope.text = attrs.text;
                    scope.amountColor = attrs.amountColor;
                    scope.amount = attrs.amount;
                }
            };
        });
})();
