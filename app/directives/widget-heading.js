/**
 * @author Batch Themes Ltd.
 * @example <widget-heading title="{{title}}" subtitle="{{subtitle}}"></widget-heading>
 */
(function() {
    'use strict';
    app.directive('widgetHeading', function(Functions, $timeout, Dashboards) {
            return {
                templateUrl: 'views/widgets/widget-heading.html',
                restrict: 'E',
                scope: {},
                link: function postLink(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.subtitle = attrs.subtitle;
                    scope.iconId = 'icon-' + Functions.random(0, 1000);
                    scope.animateButton = function(iconId) {
                        $('#' + scope.iconId).toggleClass('fa-spin');
                        $timeout(function() {
                            $('#' + scope.iconId).removeClass('fa-spin');
                        }, 2000);
                    };
                }
            };
        });
})();
