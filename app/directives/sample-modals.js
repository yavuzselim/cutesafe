/**
 * @author Batch Themes Ltd.
 * @example <sample-modals></sample-modals>
 */
(function() {
    'use strict';
   app.directive('sampleModals', function() {
            return {
                scope: {},
                templateUrl: 'views/sample-modals/sample-modals.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                }
            };
        });
})();
