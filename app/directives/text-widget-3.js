/**
 * @author Batch Themes Ltd.
 * @example <text-widget-3 title="{{title}}" subtitle="{{subtitle}}" amount="5" bg-color="white" text-color="danger"></text-widget-3>
 */
(function() {
    'use strict';
    app.directive('textWidget3', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-3.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.subtitle = attrs.subtitle;
                    scope.amount = attrs.amount;
                    scope.bgColor = attrs.bgColor;
                    scope.textColor = attrs.textColor;
                }
            };
        });
})();
