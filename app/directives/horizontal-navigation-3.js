/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.directive('horizontalNavigation3', function(Navigation, $location, $rootScope, $localStorage) {
            return {
                scope: {},
                templateUrl: 'views/horizontal-navigation/horizontal-navigation-3.html',
                restrict: 'E',
                link: function(scope, element, attrs) {

                        scope.currentUrl =  $location.absUrl().split('#!')[1];

                    scope.changeType =function(url){
                       
                        scope.currentUrl =  url;
                    }
                    scope.setLayout = function(layout) {
                        $localStorage.config.layout = layout;
                        $('body').removeClass('layout-collapsed');
                        $('body').attr('data-layout', layout);
                    };

                    var url = $location.url();
                   
                    if (url === "/") {
                        url = "/dashboard/welcome";
                    }
                    url = url.split('/');
                    url[0] = "home";
                    for (var i in url) {
                        var value = url[i];
                        value = value.replace(/-/gi, ' ');
                        value = _.capitalize(value);
                        value = value.replace(/Ui/, 'UI');
                        url[i] = value;
                    }
                    scope.url = url;
                    var items = Navigation.get();
                    var menu = [];
                    for (var i in items) {
                        for (var j in items[i].items) {
                            menu.push(items[i].items[j]);
                        }
                    }
                    scope.items = menu;
                    $rootScope.$on('$routeChangeSuccess', function() {
                        var url = $location.url();
                        if (url === "/") {
                            url = "/app/dashboard";
                        }
                        url = url.split('/');
                        url[0] = "home";
                        for (var i in url) {
                            var value = url[i];
                            value = value.replace(/-/gi, ' ');
                            value = _.capitalize(value);
                            value = value.replace(/Ui/, 'UI');
                            url[i] = value;
                        }
                        scope.url = url;
                    });
                }
            };
        });
})();
