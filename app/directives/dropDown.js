/**
 * Created by mabeh on 2.02.2018.
 */
(function () {
    'use strict';
        app.directive('bootstrapDropdown', function ($compile,$uibModal) {
        return {
            restrict: 'E',
            scope: {
                items: '=dropdownData',
                selectedItem: '=preselectedItem',
                ngModel: '=',
                coinId: '=',
                temp: '=',
                placeholder: '@',
                mask: '@',
                fieldLabel: '@'

            },
            link: function (scope, element, attrs) {


                scope.selectVal = function (item,items) {

                    if(item == 'modal'){
                        var modalInstance = $uibModal.open({
                            templateUrl : attrs['temp'] ,
                            controller : 'ModalWallet2InstanceCtrl',
                            size : 1,
                            resolve: {
                                items : function() {
                                    return [items,attrs['temp'] ,attrs['coinid']];
                                }
                            }
                        });

                        modalInstance.result.then(function(selectedItem) {
                            scope.ngModel = selectedItem;
                            scope.items.push(selectedItem);
                        }, function() {
                            //$log.info('Modal dismissed at: ' + new Date());
                        });

                    } else {
                        scope.ngModel = item;
                    }

                };





                var html = '';
                html += '<div class="form-group" style="position:relative; z-index:5;" >';
                html += '  <div class="input-group" style="width: 100%;">';
                html += '    <input required  ui-mask="{{mask}}"  name="bankAccounts" class="form-control" id="statusinput" type="text" data-ng-model ="ngModel" data-ng-attr-placeholder="{{placeholder}}" />';
                html += '    <div class="input-group-btn" >';
                html += '      <div class="btn-group " uib-dropdown is-open="scope.status.isopen">';
                html += '        <button class="btn btn-default  dropdown-toggle"   uib-dropdown-toggle ng-disabled="disabled" type="button" data-toggle="dropdown" >';
                html += '          <span class="caret"></span>';
                html += '        </button>';
                html += '        <ul  class="dropdown-menu dropdown-menu-right dropdown-scroll" role="menu">';
                html += '          <li> <a href="#" data-ng-click="selectVal(\'modal\',items)"  >Yeni Ekle </a> </li>  ' +
                    '<li class="divider"></li>';
                html+='<li data-ng-if="items.length"><i class="fa fa-spinner fa-spin" ng-show="scope.loadingBefore"></i> </li>'
                html += '          <li data-ng-repeat="item in items"><a data-ng-href="" role="menuitem" tabindex="-1" data-ng-click="selectVal(item)">{{item}}</a></li>';
                html += '        </ul>';
                html += '      </div>';
                html += '    </div>';
                html += '  </div>';
                html += '</div>';

                element.append($compile(html)(scope));
            }
        };
    });
})();