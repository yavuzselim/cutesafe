/**
 * @author Batch Themes Ltd.
 * @example <text-widget-16 title="{{title}}" amount="5" icon="{{icon}}" icon-color="white"></text-widget-16>
 */
(function() {
    'use strict';
    app.directive('textWidget16', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-16.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.textColor = attrs.textColor;
                    scope.leftColor = attrs.leftColor;
                    scope.rightColor = attrs.rightColor;
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.icon = attrs.icon;
                    scope.size = attrs.size;
                }
            };
        });
})();
