/**
 * @author Batch Themes Ltd.
 * @example <text-widget-4 title="{{title}}" label="{{label}}" amount="1234" subtitle="{{subtitle}}" percent="23" bg-color="white" text-color="danger"></text-widget-4>
 */
(function() {
    'use strict';
   app.directive('textWidget4', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-4.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.label = attrs.label;
                    scope.amount = attrs.amount;
                    scope.subtitle = attrs.subtitle;
                    scope.percent = attrs.percent;
                    scope.bgColor = attrs.bgColor;
                    scope.textColor = attrs.textColor;
                }
            };
        });
})();
