/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
   app.directive('emailSidebar', function($location) {
            return {
                templateUrl: 'views/email/email-sidebar.html',
                restrict: 'EA',
                link: function(scope) {
                    scope.hideButton = $location.path() === '/email/compose' ? true : false;
                    scope.folders = [{
                        'title': 'Inbox',
                        'icon': 'fa fa-inbox',
                        'tagClass': 'warning',
                        'total': 20,
                        'url': '/email/inbox'
                    }, {
                        'title': 'Send Mail',
                        'icon': 'fa fa-envelope-o',
                        'tagClass': false,
                        'total': 0,
                        'url': '/email/compose'
                    }, {
                        'title': 'Important',
                        'icon': 'fa fa-certificate',
                        'tagClass': 'success',
                        'total': 5,
                        'url': '/email/inbox'
                    }, {
                        'title': 'Drafts',
                        'icon': 'fa fa-file-text-o',
                        'tagClass': 'danger',
                        'total': 3,
                        'url': '/email/inbox'
                    }, {
                        'title': 'Trash',
                        'icon': 'fa fa-trash-o',
                        'tagClass': false,
                        'total': 0,
                        'url': '/email/inbox'
                    }, ];
                    scope.categories = [{
                        'title': 'Work',
                        'icon': 'fa fa-circle',
                        'tagClass': 'warning',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Documents',
                        'icon': 'fa fa-circle',
                        'tagClass': 'danger',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Social',
                        'icon': 'fa fa-circle',
                        'tagClass': 'success',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Advertising',
                        'icon': 'fa fa-circle',
                        'tagClass': 'info',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Clients',
                        'icon': 'fa fa-circle',
                        'tagClass': 'primary',
                        'url': '/email/inbox'
                    }];
                    scope.tags = [{
                        'title': 'Family',
                        'icon': 'fa fa-tag',
                        'tagClass': 'warning',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Work',
                        'icon': 'fa fa-tag',
                        'tagClass': 'info',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Home',
                        'icon': 'fa fa-tag',
                        'tagClass': 'primary',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Children',
                        'icon': 'fa fa-tag',
                        'tagClass': 'success',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Holidays',
                        'icon': 'fa fa-tag',
                        'tagClass': 'danger',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Music',
                        'icon': 'fa fa-tag',
                        'tagClass': 'warning',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Photography',
                        'icon': 'fa fa-tag',
                        'tagClass': 'info',
                        'url': '/email/inbox'
                    }, {
                        'title': 'Film',
                        'icon': 'fa fa-tag',
                        'tagClass': 'primary',
                        'url': '/email/inbox'
                    }];
                }
            };
        });
})();
