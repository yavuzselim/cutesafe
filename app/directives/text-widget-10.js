/**
 * @author Batch Themes Ltd.
 * @example <text-widget-10 text-align="left" title="{{title}}" amount="5" icon="{{icon}}" icon-color="white" circle-color="danger"></text-widget-10>
 */
(function() {
    'use strict';
   app.directive('textWidget10', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-10.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.size = attrs.size;
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.icon = attrs.icon;
                    scope.iconColor = attrs.iconColor;
                    scope.textColor = attrs.textColor;
                    scope.leftColor = attrs.leftColor;
                    scope.rightColor = attrs.rightColor;
                    scope.circleBorderColor = attrs.circleBorderColor;
                }
            };
        });
})();
