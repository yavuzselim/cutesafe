/*
 * @author https://github.com/sparkalow/angular-count-to
 **/
(function() {
    'use strict';
   app.directive('countToPercent', ['$timeout', '$filter',
            function($timeout, $filter) {
                return {
                    replace: false,
                    scope: true,
                    link: function(scope, element, attrs) {
                        var e = element[0];
                        var num, refreshInterval, duration, steps, step, countToPercent, value, increment;
                        var calculate = function() {
                            refreshInterval = 30;
                            step = 0;
                            scope.timoutId = null;
                            countToPercent = parseInt(attrs.countToPercent) || 0;
                            scope.value = parseInt(attrs.value, 10) || 0;
                            duration = (parseFloat(attrs.duration) * 1000) || 0;
                            steps = Math.ceil(duration / refreshInterval);
                            increment = ((countToPercent - scope.value) / steps);
                            num = scope.value;
                        };
                        var tick = function() {
                            scope.timoutId = $timeout(function() {
                                num += increment;
                                step++;
                                if (step >= steps) {
                                    $timeout.cancel(scope.timoutId);
                                    num = countToPercent;
                                    countToPercent = $filter('currency')(countToPercent, '', 0) + '%';
                                    e.textContent = countToPercent;
                                } else {
                                    e.textContent = $filter('currency')(Math.round(num), '', 0) + '%';
                                    tick();
                                }
                            }, refreshInterval);
                        };
                        var start = function() {
                            if (scope.timoutId) {
                                $timeout.cancel(scope.timoutId);
                            }
                            calculate();
                            tick();
                        };
                        attrs.$observe('countToPercent', function(val) {
                            if (val) {
                                start();
                            }
                        });
                        attrs.$observe('value', function(val) {
                            start();
                        });
                        return true;
                    }
                };
            }
        ]);
})();
