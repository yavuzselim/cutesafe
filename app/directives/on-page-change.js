/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
   app.directive('onPageChange', function($rootScope, $location, $localStorage, Functions) {
            return {
                restrict: 'A',
                link: function(scope, elem, attrs) {
                    var emptyView = [
                        '/extras/invoice'
                    ];
                    var fullsizeBackgroundImage = [
                        '/pages/home',
                        '/pages/error-page',
                        '/pages/forgot-password',
                        '/pages/lock-screen',
                        '/pages/login',
                        '/pages/register',
                        '/pages/coming-soon',
                        '/pages/under-maintenance',
                    ];
                    var config = $localStorage.config;
                    var layout = config.layout;
                    var url = $location.url();
                    $rootScope.$on('$routeChangeStart', function() {
                        Pace.restart();
                        url = $location.url();
                        if (emptyView.indexOf(url) !== -1) {
                            $('body').attr('data-layout', 'empty-view');
                        } else if (fullsizeBackgroundImage.indexOf(url) !== -1) {
                            $('body').attr('data-layout', 'fullsize-background-image');
                        } else {
                            $('body').attr('data-layout', layout);
                        }
                        //$('.section-link').removeClass('sideline-active');
                    });
                    $rootScope.$on('$routeChangeSuccess', function() {
                        url = $location.url();
                        if (emptyView.indexOf(url) !== -1) {
                            $('body').attr('data-layout', 'empty-view');
                        } else if (fullsizeBackgroundImage.indexOf(url) !== -1) {
                            $('body').attr('data-layout', 'fullsize-background-image');
                            $('.fullsize-background-image-1').addClass('animated fadeIn delay-1000');
                        } else {
                            $('body').attr('data-layout', layout);
                        }
                        //close left sidebar
                        var layout = $('body').data('layout');
                        //console.log(layout);
                        var isCollapsed = $('body').hasClass('layout-collapsed')
                        if (layout === 'left-sidebar-over' && isCollapsed) {
                            $('body').toggleClass('layout-collapsed');
                        }
                        var el = angular.element('.main');
                        var wh = $(window).height();
                        el.css('min-height', wh + 'px');
                        /*
                        var section = url.split('/')[1];
                        $('.section-' + section).addClass('active');
                        $('.section-link-' + section).addClass('sideline-active');
                        */
                    });
                }
            };
        });
})();
