/**
 * @author Batch Themes Ltd.
 * @example <text-widget-19 text-color="white" text="fa fa-heart-o" amount-color="white" amount="19"></text-widget-19>
 */
(function() {
    'use strict';
    app.directive('textWidget19', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-19.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.textColor = attrs.textColor;
                    scope.text = attrs.text;
                    scope.amountColor = attrs.amountColor;
                    scope.amount = attrs.amount;
                }
            };
        });
})();
