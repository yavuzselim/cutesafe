/**
 * @author Batch Themes Ltd.
 * @example <text-widget-8 title="{{title}}" amount="1234" percent="23" align="left" bg-color="white" text-color="success" percent-color="{{success}}"></text-widget-8>
 */
(function() {
    'use strict';
    app.directive('textWidget8', function() {
            return {
                scope: {},
                templateUrl: 'views/text-widgets/text-widget-8.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                    scope.title = attrs.title;
                    scope.amount = attrs.amount;
                    scope.percent = attrs.percent;
                    scope.align = attrs.align;
                    scope.bgColor = attrs.bgColor;
                    scope.textColor = attrs.textColor;
                    scope.percentColor = attrs.percentColor;
                }
            };
        });
})();
