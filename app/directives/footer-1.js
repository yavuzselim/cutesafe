/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.directive('footer1', function() {
            return {
                scope: {},
                templateUrl: 'views/footer/footer-1.html',
                restrict: 'E',
                link: function(scope, element, attrs) {
                }
            };
        });
})();
