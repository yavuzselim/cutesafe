/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.service('Layouts', function() {
            function layout(layout, name) {
                return {
                    layout: layout,
                    name: name
                }
            };
            var layouts = [
                layout('default-sidebar', 'Default sidebar'),
                layout('collapsed-sidebar', 'Collapsed sidebar'),
                layout('off-canvas', 'Off canvas'),
                layout('left-sidebar-over', 'Left sidebar over'),
                layout('left-sidebar-under', 'Left sidebar under'),
                layout('horizontal-navigation-1', 'Horizontal navigation 1'),
                layout('horizontal-navigation-2', 'Horizontal navigation 2'),
                layout('horizontal-navigation-3', 'Horizontal navigation 3'),
                layout('mega-menu', 'Mega menu')
            ];
            return {
                getLayouts: function() {
                    return layouts;
                }
            };
        });
})();
