/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.service('Functions', function() {
            //http://www.sitepoint.com/javascript-generate-lighter-darker-color/
            var colorLuminance = function(hex, lum) {
                // validate hex string
                hex = String(hex).replace(/[^0-9a-f]/gi, '');
                if (hex.length < 6) {
                    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                }
                lum = lum || 0;
                // convert to decimal and change luminosity
                var rgb = "#",
                    c, i;
                for (i = 0; i < 3; i++) {
                    c = parseInt(hex.substr(i * 2, 2), 16);
                    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                    rgb += ("00" + c).substr(c.length);
                }
                return rgb;
            };
            return {
                toggleSection: function(id) {
                    $('.section-' + id).toggleClass('active');
                    $('.fa-caret-down.icon-' + id).toggleClass('fa-rotate-180');
                },
                incrementingData: function() {
                    var series = [];
                    var labels = [];
                    for (var x = 0; x < 50; x++) {
                        if (x % 2 === 0) {
                            continue;
                        }
                        labels.push('Label ' + x);
                        series.push(Functions.random(x, x + 10));
                    }
                    return {
                        series: series,
                        labels: labels
                    }
                },
                decrementingData: function() {
                    var series = [];
                    var labels = [];
                    for (var x = 50; x > 0; x--) {
                        if (x % 2 === 0) {
                            continue;
                        }
                        labels.push('Label ' + x);
                        series.push(Functions.random(x + 10, x));
                    }
                    return {
                        series: series,
                        labels: labels
                    }
                },
                randomData: function() {
                    var series = [];
                    var labels = [];
                    for (var x = 0; x < 30; x++) {
                        labels.push('Label ' + x);
                        series.push(Functions.random(20, 80));
                    }
                    return {
                        series: series,
                        labels: labels
                    }
                },
                reverseArray: function(input) {
                    var ret = new Array;
                    for (var i = input.length - 1; i >= 0; i--) {
                        ret.push(input[i]);
                    }
                    return ret;
                },
                random: function(min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                },
                lighten: function(col, amt) {
                    amt = Math.abs(amt);
                    amt = amt / 100;
                    return colorLuminance(col, amt);
                },
                darken: function(col, amt) {
                    amt = Math.abs(amt);
                    amt = (amt / 100) * -1;
                    return colorLuminance(col, amt);
                },
                detectIE: function() {
                    /**
                     * detect IE
                     * returns version of IE or false, if browser is not Internet Explorer
                     */
                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf('MSIE ');
                    if (msie > 0) {
                        // IE 10 or older => return version number
                        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                    }
                    var trident = ua.indexOf('Trident/');
                    if (trident > 0) {
                        // IE 11 => return version number
                        var rv = ua.indexOf('rv:');
                        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                    }
                    var edge = ua.indexOf('Edge/');
                    if (edge > 0) {
                        // Edge (IE 12+) => return version number
                        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                    }
                    // other browser
                    return false;
                }
            };
        });
})();
