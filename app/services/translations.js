/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.service('Translations', function() {
            var en = {
                'title': 'Hello, good friend!',
                'newtext': 'New',
                'navigation': 'Navigation',
                'post': 'Post',
                'message': 'Message',
                'account': 'Account',
                'inbox': 'Inbox',
                'profile': 'Profile',
                'settings': 'Settings',
                'lock': 'Lock account',
                'logout': 'Logout',
                'english': 'English',
                'french': 'French',
                'spanish': 'Spanish',
                'german': 'German',
            };
            var de = {
                'title': 'Hallo, guter Freund!',
                'newtext': 'Neu',
                'navigation': 'Kategorien',
                'post': 'Blogeintrag',
                'message': 'Nachricht',
                'account': 'Konto',
                'inbox': 'Email',
                'profile': 'Profil',
                'settings': 'Einstellungen',
                'lock': 'Schloss Konto',
                'logout': 'Ausloggen',
                'english': 'Englisch',
                'french': 'Französisch',
                'spanish': 'Spanisch',
                'german': 'Deutsche',
            };
            var es = {
                'title': 'Hola amigo!',
                'newtext': 'Nuevo',
                'navigation': 'Categorías',
                'post': 'Artículo',
                'message': 'Mensaje',
                'account': 'Cuenta',
                'inbox': 'Correo',
                'profile': 'Perfil',
                'settings': 'Confuguración',
                'lock': 'Bloquear cuenta',
                'logout': 'Cerrar sesión',
                'english': 'Inglés',
                'french': 'Frances',
                'spanish': 'Español',
                'german': 'Alemán',
            };
            var fr = {
                'title': 'Bonjour, bon ami!',
                'newtext': 'Neuf',
                'navigation': 'Catégories',
                'post': 'Message',
                'message': 'Entrée',
                'account': 'Compte',
                'inbox': 'Email',
                'profile': 'Profil',
                'settings': 'Paramètres',
                'lock': 'Bloquer compte',
                'logout': 'Se déconnecter',
                'english': 'Anglais',
                'french': 'Français',
                'spanish': 'Espanol',
                'german': 'Allemand',
            };
            return {
                getTranslations: function(lang) {
                    var keys = en;
                    if (lang === 'fr') {
                        keys = angular.copy(fr);
                    }
                    if (lang === 'de') {
                        keys = angular.copy(de);
                    }
                    if (lang === 'es') {
                        keys = angular.copy(es);
                    }
                    return keys;
                },
                translate: function(key, lang) {
                    var keys = en;
                    if (lang === 'fr') {
                        keys = angular.copy(fr);
                    }
                    if (lang === 'de') {
                        keys = angular.copy(de);
                    }
                    if (lang === 'es') {
                        keys = angular.copy(es);
                    }
                    var translation = angular.isDefined(keys[key]) ? keys[key] : false;
                    return translation;
                }
            };
        });
})();
