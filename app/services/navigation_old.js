/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.service('Navigation', function() {
            return {
                get: function() {
                    return [{
                        "id": "navigation",
                        "title": "Navigation",
                        "url": false,
                        "icon": false,
                        "number": {},
                        "label": {},
                        "items": [{
                            "id": "musteriler",
                            "title": "Müşteriler",
                            "url": "app.dashboard",
                            "icon": "/_assets/images/musteri.png",
                            "number": {},
                            "label": {},
                            "items": []
                        },{
                            "id": "bayiler",
                            "title": "Bayiler",
                            "url": "app.bayiler",
                            "icon": "/_assets/images/bayi.png",
                            "number": {},
                            "label": {},
                            "items": []
                        },{
                                "id": "subeler",
                                "title": "Şubeler",
                                "url": "app.subeler",
                                "icon": "/_assets/images/bayi.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "cariler",
                                "title": "Cari Hesaplar",
                                "url": "app.cariler",
                                "icon": "/_assets/images/cari-hesap.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "siparis",
                                "title": "Siparişler",
                                "url": "app.siparisler",
                                "icon": "/_assets/images/siparis.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "fisler",
                                "title": "Fişler",
                                "url": "app.fisler",
                                "icon": "/_assets/images/receipt.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "harita",
                                "title": "Harita",
                                "url": "app.harita",
                                "icon": "/_assets/images/harita.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "cihazlar",
                                "title": "Cihazlar",
                                "url": "app.cihazlar",
                                "icon": "/_assets/images/cihazlar.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "sim",
                                "title": "Sim",
                                "url": "app.sim",
                                "icon": "/_assets/images/sim.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },{
                                "id": "kisiler",
                                "title": "Kişiler",
                                "url": "app.kisiler",
                                "icon": "/_assets/images/persons.png",
                                "number": {},
                                "label": {},
                                "items": []
                        },
                            {
                            "id": "sistem",
                            "title": "Sistem Tanımlamaları",
                            "url": false,
                            "icon": "/_assets/images/sistem.png",
                            "number": {},
                            "label": {},
                            "items": [
                                {
                                "id": "arac_modelleri",
                                "title": "Araç Modelleri",
                                "url": "sistem.aracModelleri",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 }, {
                                "id": "arac_markalari",
                                "title": "Araç Markaları",
                                "url": "sistem.aracMarkalari",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 }, {
                                "id": "cihaz_markalari",
                                "title": "Cihaz Markaları",
                                "url": "sistem.cihazMarkalari",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },{
                                "id": "cihaz_modelleri",
                                "title": "Cihaz Modelleri",
                                "url": "sistem.cihazModelleri",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },{
                                "id": "bankalar",
                                "title": "Bankalar",
                                "url": "sistem.bankalar",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },{
                                "id": "gsm_tarifeleri",
                                "title": "GSM Tarifeleri",
                                "url": "sistem.gsmTarifeleri",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },{
                                "id": "kullanici_gruplari",
                                "title": "Kullanıcı Gruplar",
                                "url": "sistem.gruplar",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },{
                                "id": "kullanicilar",
                                "title": "Kullanıcılar",
                                "url": "sistem.kullanicilar",
                                "icon": false,
                                "number": {},
                                "label": {},
                                "items": []
                                 },
                            ]
                        },{
                                "id": "logout",
                                "title": "Çıkış",
                                "url": "app.logout",
                                "icon": "/_assets/images/logout.png",
                                "number": {},
                                "label": {},
                                "items": []
                            }



                        ]
                    }]
                }
            };
        });
})();
