/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.service('SampleData', function() {
            var messages = [];
            var githubActivity = [{
                "date": "A few seconds ago",
                "title": "Lucas Smith checked out branch feature/123-branch at Themes/greatTheme",
                "description": "2 commits with 22 additions and 2 deletions",
                "name": "Lucas Smith",
                "img": "/assets/faces/m1.png"
            }, {
                "date": "2 hours ago",
                "title": "George Clinton pushed to bug/123-branch at Themes/greatTheme",
                "description": "2 commits with 22 additions and 2 deletions",
                "name": "George Clinton",
                "img": "/assets/faces/m5.png"
            }, {
                "date": "3 hours ago",
                "title": "Jane Perez pushed to branch/223-branch at Themes/greatTheme",
                "description": "4 commits with 36 additions and 16 deletions",
                "name": "Jane Perez",
                "img": "/assets/faces/w3.png"
            }, {
                "date": "4 hours ago",
                "title": "Michael Smith pushed to feature/223-branch at Themes/greatTheme",
                "description": "4 commits with 16 additions and 13 deletions",
                "name": "Michael Smith",
                "img": "/assets/faces/m2.png"
            }, {
                "date": "5 hours ago",
                "title": "Gladys Schuster closed issue #1234",
                "description": "q80c81c - fixed error. Emails not being sent",
                "name": "Gladys Schuster",
                "img": "/assets/faces/w5.png"
            }];
            var timeline = [{
                "when": "10 minutes ago",
                "text": "Someone commented on your post",
                "color": "info",
                "position": "left"
            }, {
                "when": "15 minutes ago",
                "text": "Made a sale in Themeforest",
                "color": "success",
                "position": "right"
            }, {
                "when": "18 minutes ago",
                "text": "A friend posted something on instagram",
                "color": "info",
                "position": "right"
            }, {
                "when": "22 minutes ago",
                "text": "Fixed github issue",
                "color": "danger",
                "position": "right"
            }, {
                "when": "30 minutes ago",
                "text": "Finished important task",
                "color": "success",
                "position": "left"
            }, {
                "when": "2 hours ago",
                "text": "Finished integration tests",
                "color": "danger",
                "position": "right"
            }, {
                "when": "3 hours ago",
                "text": "Prepared trip to Jupiter",
                "color": "warning",
                "position": "left"
            }, {
                "when": "6 hours ago",
                "text": "Pushed commit to github",
                "color": "info",
                "position": "right"
            }, {
                "when": "2 days ago",
                "text": "Posted something in pinterest",
                "color": "info",
                "position": "left"
            }, {
                "when": "3 days ago",
                "text": "Created issue in github",
                "color": "warning",
                "position": "right"
            }, {
                "when": "5 days ago",
                "text": "Liked something in facebook",
                "color": "success",
                "position": "left"
            }, {
                "when": "A month ago",
                "text": "Tweeted about new restautant",
                "color": "danger",
                "position": "right"
            }, {
                "when": "2 months ago",
                "text": "Commented on recent trip to Alaska",
                "color": "warning",
                "position": "left"
            }, {
                "when": "3 months ago",
                "text": "Went to Australia",
                "color": "info",
                "position": "right"
            }];
            var stocks = [{
                "name": "Apple Inc.",
                "ticker": "AAPL",
                "price": 115.52,
                "value": 0.39,
                "variation": 0.34
            }, {
                "name": "Google Inc.",
                "ticker": "GOOG",
                "price": 635.3,
                "value": -7.38,
                "variation": -1.15
            }, {
                "name": "Microsoft Corporation",
                "ticker": "MSFT",
                "price": 46.74,
                "value": 0.12,
                "variation": 0.26
            }, {
                "name": "LinkedIn Corp",
                "ticker": "LNKD",
                "price": 190.04,
                "value": 0.53,
                "variation": 0.28
            }, {
                "name": "Facebook Inc.",
                "ticker": "FB",
                "price": 94.3,
                "value": -0.82,
                "variation": -0.86
            }];
            var salesStream = [{
                "tag": "success",
                "type": "charge",
                "entity": "Michael Smith",
                "action": "upgraded to",
                "amount": 50,
                "date": "4 minutes ago"
            }, {
                "tag": "primary",
                "type": "charge",
                "entity": "Jane Perez",
                "action": "cancelled",
                "amount": 25,
                "date": "34 minutes ago"
            }, {
                "tag": "primary",
                "type": "upgrade",
                "entity": "George Clinton from Facebook",
                "action": "upgraded to",
                "amount": 25,
                "date": "an hour ago"
            }, {
                "tag": "warning",
                "type": "cancelled",
                "entity": "Someone from Google",
                "action": "cancelled",
                "amount": 25,
                "date": "2 hours ago"
            }, {
                "tag": "success",
                "type": "upgrade",
                "entity": "Lucas Smith",
                "action": "upgraded to",
                "amount": 25,
                "date": "2 hours ago"
            }, {
                "tag": "info",
                "type": "upgrade",
                "entity": "Someone from Apple",
                "action": "upgraded to",
                "amount": 50,
                "date": "3 hours ago"
            }, {
                "tag": "info",
                "type": "cancelled",
                "entity": "Tom Hanks",
                "action": "cancelled",
                "amount": 50,
                "date": "3 hours ago"
            }, {
                "tag": "success",
                "type": "switch",
                "entity": "Jennifer Aniston",
                "action": "subscribed to",
                "amount": 50,
                "date": "4 hours ago"
            }, {
                "tag": "danger",
                "type": "switch",
                "entity": "George Clooney",
                "action": "upgraded to",
                "amount": 50,
                "date": "4 hours ago"
            }, {
                "tag": "success",
                "type": "switch",
                "entity": "Mickey mouse",
                "action": "subscribed to",
                "amount": 10,
                "date": "5 hours ago"
            }];
            var poll = [{
                "tag": "warning",
                "progressBar": "danger",
                "barValue": 63,
                "amount": 103,
                "title": "Spain"
            }, {
                "tag": "info",
                "progressBar": "success",
                "barValue": 76,
                "amount": 204,
                "title": "Canada"
            }, {
                "tag": "info",
                "progressBar": "info",
                "barValue": 79,
                "amount": 557,
                "title": "South Africa"
            }, {
                "tag": "danger",
                "progressBar": "danger",
                "barValue": 67,
                "amount": 754,
                "title": "Russia"
            }, {
                "tag": "warning",
                "progressBar": "warning",
                "barValue": 37,
                "amount": 486,
                "title": "China"
            }, {
                "tag": "success",
                "progressBar": "success",
                "barValue": 87,
                "amount": 457,
                "title": "Canada"
            }, {
                "tag": "primary",
                "progressBar": "primary",
                "barValue": 17,
                "amount": 176,
                "title": "Mexico"
            }, {
                "tag": "danger",
                "progressBar": "danger",
                "barValue": 72,
                "amount": 72,
                "title": "India"
            }, {
                "tag": "info",
                "progressBar": "info",
                "barValue": 37,
                "amount": 486,
                "title": "Argentina"
            }, {
                "tag": "success",
                "progressBar": "success",
                "barValue": 33,
                "amount": 46,
                "title": "Belgium"
            }];
            var users = [{
                "name": "Lucas smith",
                "company": "Apple, Inc.",
                "description": "Vital Database Dude",
                "img": "/assets/faces/m1.png",
                "color": "success",
                "country": "Australia"
            }, {
                "name": "Janet Abshire",
                "company": "Pinterest",
                "description": "Lead Innovation Officer",
                "img": "/assets/faces/w1.png",
                "color": "info",
                "country": "USA"
            }, {
                "name": "Lucas Koch",
                "company": "Reddit",
                "description": "Incomparable UX Editor",
                "img": "/assets/faces/m2.png",
                "color": "success",
                "country": "England"
            }, {
                "name": "Gladys Schuster",
                "description": "Primary Product Pioneer",
                "company": "Coursera",
                "img": "/assets/faces/w2.png",
                "color": "warning",
                "country": "USA"
            }, {
                "name": "George Clinton",
                "company": "Facebook",
                "description": "World Class API Genius",
                "img": "/assets/faces/m3.png",
                "color": "success",
                "country": "Canada"
            }, {
                "name": "Jennifer Weber",
                "description": "World Class Innovation Controller",
                "company": "Lyft",
                "img": "/assets/faces/w3.png",
                "color": "info",
                "country": "New Zealand"
            }, {
                "name": "Thomas Jefferson",
                "description": "Client Side Press Tester",
                "company": "Quora",
                "img": "/assets/faces/m4.png",
                "color": "success",
                "country": "USA"
            }, {
                "name": "Courtney Dickens",
                "description": "Principal Integration Ninja",
                "company": "Yelp",
                "img": "/assets/faces/w5.png",
                "color": "danger",
                "country": "Australia"
            }, {
                "name": "James Smith",
                "description": "Project Management Champ",
                "company": "Apple, Inc.",
                "img": "/assets/faces/m6.png",
                "color": "primary",
                "country": "Germany"
            }, {
                "name": "Clorinda Murphy",
                "description": "Social Media Writer",
                "company": "Tinder",
                "img": "/assets/faces/w6.png",
                "color": "warning",
                "country": "Canada"
            }, {
                "name": "Eric Simpson",
                "description": "Innovation Pioneer",
                "company": "Facebook",
                "img": "/assets/faces/m7.png",
                "color": "danger",
                "country": "USA"
            }, {
                "name": "Monica White",
                "description": "iOS Strategist",
                "company": "Twitter",
                "img": "/assets/faces/w7.png",
                "color": "success",
                "country": "Argentina"
            }, {
                "name": "Michael Jenkins",
                "description": "Ruby On Rails Developer",
                "company": "Microsoft Inc.",
                "img": "/assets/faces/m8.png",
                "color": "info",
                "country": "Brazil"
            }, {
                "name": "Jackie Perkins",
                "description": "Executive PR Evangelist",
                "company": "Uber",
                "img": "/assets/faces/w8.png",
                "color": "danger",
                "country": "England"
            }, {
                "name": "Melvin Hicks",
                "description": "Primary PHP Monkey",
                "company": "Google Inc.",
                "img": "/assets/faces/m9.png",
                "color": "warning",
                "country": "USA"
            }, {
                "name": "Hannah Cook",
                "description": "Wise Email Captain",
                "company": "Pinterest",
                "img": "/assets/faces/w9.png",
                "color": "success",
                "country": "Sweden"
            }, {
                "name": "Jerome Lynch",
                "description": "Android Engineer",
                "company": "LG Electronics",
                "img": "/assets/faces/m10.png",
                "color": "danger",
                "country": "France"
            }, {
                "name": "Kirsten Perez",
                "description": "Project Management Researcher",
                "company": "Facebook",
                "img": "/assets/faces/w10.png",
                "color": "danger",
                "country": "Mexico"
            }];
            var transactions = [{
                "date": "Aug 15, 2015",
                "country": "Australia",
                "tagText": "Deposit",
                "tagColor": "warning",
                "amount": 435.82
            }, {
                "date": "Jun 11, 2015",
                "country": "Spain",
                "tagText": "Withdrawal",
                "tagColor": "danger",
                "amount": 135.11
            }, {
                "date": "Apr 2, 2015",
                "country": "Canada",
                "tagText": "Payment",
                "tagColor": "success",
                "amount": 335.39
            }, {
                "date": "Apr 1, 2015",
                "country": "France",
                "tagText": "Payment",
                "tagColor": "info",
                "amount": 535.99
            }, {
                "date": "Mar 25, 2015",
                "country": "Mexico",
                "tagText": "Deposit",
                "tagColor": "primary",
                "amount": 465.11
            }, {
                "date": "Mar 12, 2015",
                "country": "USA",
                "tagText": "Payment",
                "tagColor": "warning",
                "amount": 935.32
            }, {
                "date": "Feb 26, 2015",
                "country": "Argentina",
                "tagText": "Withdrawal",
                "tagColor": "success",
                "amount": 35.52
            }, {
                "date": "Feb 22, 2015",
                "country": "China",
                "tagText": "Withdrawal",
                "tagColor": "success",
                "amount": 45.99
            }, {
                "date": "Jan 12, 2015",
                "country": "Japan",
                "tagText": "Deposit",
                "tagColor": "danger",
                "amount": 95.99
            }, {
                "date": "Jan 7, 2015",
                "country": "South Africa",
                "tagText": "Payment",
                "tagColor": "info",
                "amount": 544.01
            }];
            var addresses = [{
                address: '1 Hacker Way',
                location: 'Menlo Park, CA 94025'
            }, {
                address: '1 Infinite Loop',
                location: 'Cupertino, CA 95014'
            }, {
                address: '1600 Amphitheatre Parkway',
                location: 'Mountain View, CA 94043'
            }];
            var todoList = [{
                text: 'Learn AngularJS',
                done: false
            }, {
                text: 'Check out branch from github',
                done: false
            }, {
                text: 'Build Android app',
                done: false
            }, {
                text: 'Call client',
                done: false
            }];
            var pricingTables = {
                'basic': [{
                    "icon": "fa fa-check",
                    "text": "Free setup",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Unlimited bandwidth",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "100 GB storage",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "5 user accounts",
                    "color": "success"
                }, {
                    "icon": "fa fa-remove",
                    "text": "0% transaction fee",
                    "color": "danger"
                }, {
                    "icon": "fa fa-remove",
                    "text": "Free support",
                    "color": "danger"
                }, {
                    "icon": "fa fa-remove",
                    "text": "Unlimited upgrades",
                    "color": "danger"
                }],
                'professional': [{
                    "icon": "fa fa-check",
                    "text": "Free setup",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Unlimited bandwidth",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "200 GB storage",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "10 user accounts",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "0% transaction fee",
                    "color": "success"
                }, {
                    "icon": "fa fa-remove",
                    "text": "Free support",
                    "color": "danger"
                }, {
                    "icon": "fa fa-remove",
                    "text": "Unlimited upgrades",
                    "color": "danger"
                }],
                'business': [{
                    "icon": "fa fa-check",
                    "text": "Free setup",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Unlimited bandwidth",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "1 TB storage",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Unlimited user accounts",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "0% transaction fee",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Free support",
                    "color": "success"
                }, {
                    "icon": "fa fa-check",
                    "text": "Unlimited upgrades",
                    "color": "success"
                }]
            };
            var products = [{
                'sku': 63157685,
                'name': 'Panasonic DMC-ZS60S LUMIX 4K Digital Camera 18 Megapixels',
                'by': 'Panasonic',
                'price': 447.99,
                'rating': 5,
                'img': 'p1.png'
            }, {
                'sku': 23147621,
                'name': 'GoPro HERO4 SILVER',
                'by': 'GoPro',
                'price': 399.95,
                'rating': 4,
                'img': 'p2.png'
            }, {
                'sku': 13125111,
                'name': 'Nikon Coolpix L340 Digital Camera',
                'by': 'Nikon',
                'price': 131,
                'rating': 5,
                'img': 'p3.png'
            }, {
                'sku': 63123111,
                'name': 'Fujifilm FinePix XP80 Waterproof Digital Camera with 2.7-Inch LCD',
                'by': 'Fujifilm',
                'price': 154.94,
                'rating': 3,
                'img': 'p10.png'
            }, {
                'sku': 22125151,
                'name': 'Sony DSCW800/B 20.1 MP Digital Camera ',
                'by': 'Sony',
                'price': 100,
                'rating': 4,
                'img': 'p5.png'
            }, {
                'sku': 93125191,
                'name': 'Canon PowerShot SX400 Digital Camera with 30x Optical Zoom',
                'by': 'Canon',
                'price': 124,
                'rating': 5,
                'img': 'p6.png'
            }, {
                'sku': 777125111,
                'name': 'Nikon COOLPIX S33 Waterproof Digital Camera',
                'by': 'Nikon',
                'price': 106.95,
                'rating': 4,
                'img': 'p7.png'
            }, {
                'sku': 888251331,
                'name': 'Sony DSCW830 20.1 MP Digital Camera with 2.7-Inch LCD',
                'by': 'Sony',
                'price': 128,
                'rating': 5,
                'img': 'p8.png'
            }, {
                'sku': 98945745,
                'name': 'Nikon COOLPIX AW130 Waterproof Digital Camera with Built-In Wi-Fi ',
                'by': 'Nikon',
                'price': 279.11,
                'rating': 5,
                'img': 'p9.png'
            }, {
                'sku': 33425131,
                'name': 'Uncharted 4',
                'by': 'Sony',
                'price': 59.88,
                'rating': 3,
                'img': 'p4.png'
            }];
            var browsers = [{
                "name": "Google Chrome",
                "icon": "fa fa-chrome",
                "amount": 435
            }, {
                "name": "Firefox",
                "icon": "fa fa-firefox",
                "amount": 223
            }, {
                "name": "Internet Explorer 11",
                "icon": "fa fa-internet-explorer",
                "amount": 235
            }, {
                "name": "Safari",
                "icon": "fa fa-safari",
                "amount": 150
            }, {
                "name": "Internet Explorer 10",
                "icon": "fa fa-internet-explorer",
                "amount": 88
            }, {
                "name": "Internet Explorer 9",
                "icon": "fa fa-internet-explorer",
                "amount": 35
            }, {
                "name": "Opera",
                "icon": "fa fa-opera",
                "amount": 20
            }, ];
            return {
                getProducts: function() {
                    return products;
                },
                getGithubActivity: function() {
                    return githubActivity;
                },
                getTimeline: function() {
                    return timeline;
                },
                getStocks: function() {
                    return stocks;
                },
                getSalesStream: function() {
                    return salesStream;
                },
                getPoll: function() {
                    return poll;
                },
                getUsers: function() {
                    return users;
                },
                getTransactions: function() {
                    return transactions;
                },
                getAddresses: function() {
                    return addresses;
                },
                getTodoList: function() {
                    return todoList;
                },
                getPricingTables: function() {
                    return pricingTables;
                },
                getBrowsers: function() {
                    return browsers;
                },
                getMessages: function() {
                    return messages;
                },
                setMessages: function(message) {
                    return messages.push(message);
                }
            };
        });
})();
