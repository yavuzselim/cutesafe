/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
    app.filter('searchAndReplace', function() {
            return function(input, search, replace) {
                for (var i = 0; i < search.length; i++) {
                    if (input === search[i]) {
                        input = replace[i];
                    }
                }
                return input;
            };
        });
})();
