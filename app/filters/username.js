/**
 * @author Batch Themes Ltd.
 */
(function() {
    'use strict';
   app.filter('username', function() {
            return function(input) {
                if (!input) {
                    return false;
                }
                return input.toLowerCase().replace(/\s/gi, '.');
            };
        });
})();
