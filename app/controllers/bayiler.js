/**
 * Created by mabeh on 7.03.2018.
 */
app.controller("DemoInstance2", [
  "$scope",
  "$rootScope",
  "dataProvider",
  "toaster",
  "$uibModalInstance",
  "items",
  function(
    $scope,
    $rootScope,
    dataProvider,
    toaster,
    $uibModalInstance,
    items
  ) {
    var type = items[0];
    var temp = items[1];
    var modulName = items[2];
    var point = $rootScope.endPoints;
    var component = point[modulName][temp][type];
    $scope.template = component.temp;

    console.log($scope.template);
    $scope.ok = function() {
      $scope.myModel = {};
      $scope.master = $scope.myModel;

      $scope.form = {
        submit: function(form) {
          form.$setPristine(false);
          if (form.$invalid) {
            angular.forEach(form.$error.required, function(field) {
              field.$setDirty();
            });

            return;
          } else {
            var newData = {};

            dataProvider.sendData(
              component.url,
              newData,
              component.method,
              function(resp) {
                $scope.loadingBefore = false;
                var response = resp.data;
                if (response.success) {
                  $scope.toaster = {
                    type: "success",
                    title: "Alış Emri Açıldı.",
                    text: "Alış emriniz başarılı bir şekilde oluşturuldu."
                  };

                  toaster.pop(
                    $scope.toaster.type,
                    $scope.toaster.title,
                    $scope.toaster.text
                  );

                  //$uibModalInstance.close({});
                } else {
                  $scope.toaster = {
                    type: "error",
                    title: "Emir Oluşturulamadı.",
                    text: response.message
                  };
                  toaster.pop(
                    $scope.toaster.type,
                    $scope.toaster.title,
                    $scope.toaster.text
                  );
                }
              }
            );
          }
        },
        reset: function(form) {
          $scope.myModel = angular.copy($scope.master);
          form.$setPristine(true);
        }
      };
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
  }
]);
app.controller("bayilerCtrl", [
  "$scope",
  "$rootScope",
  "NgTableParams",
  "dataProvider",
  "toaster",
  "$uibModal",
  "JS_REQUIRES",
  "$aside",
  function(
    $scope,
    $rootScope,
    NgTableParams,
    dataProvider,
    toaster,
    $uibModal,
    jsRequires,
    $aside
  ) {
    $scope.type = "Kurumsal";
    $scope.typem = "Corporate";

    $scope.modul_baslik = "BAYİLERqw";

    $scope.getList = function(type, skip, take) {
      $scope.tableParams = { data: {} };
      var point = $rootScope.endPoints;
      var component = point.bayiler;

      dataProvider.sendData(
        component.getList(type, skip, take),
        {},
        "GET",
        function(response, error) {
          var resp = response.data;
          if (!error) {
            var datams = resp.Result.EntityList;

            if (resp.ResponseCode == "Successfull") {
              $scope.loadingBefore = true;
              $scope.tableParams = {
                data: datams
              };
            } else {
              $scope.tableParams = {
                data: {}
              };

              $scope.toaster = {
                type: "error",
                title: "Kayıt Bulunamadı",
                text: resp.Message
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          } else {
            if (resp) {
              if (resp.ResponseCode == 1100) {
                $scope.toaster = {
                  type: "error",
                  title: "Kayıt Bulunamadı",
                  text: resp.Message
                };
              } else {
                $scope.toaster = {
                  type: "error",
                  title: "Bayiler Listesi Yüklenemedi",
                  text:
                    "Bayiler Listesi yüklenemedi.Lütfen Daha Sonra Tekrar Deneyin"
                };
              }

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            } else {
              $scope.toaster = {
                type: "error",
                title: "Bayiler Listesi Yüklenemedi",
                text:
                  "Bayiler Listesi yüklenemedi. Lütfen Daha Sonra Tekrar Deneyin"
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          }
        }
      );
    };

    $scope.changeType = function(type) {
      var name = "Kurumsal";
      if (type == "Corporate") {
        name = "Kurumsal";
      } else if (type == "Personal") {
        name = "Bireysel";
      } else if (type == "Foreigner") {
        name = "Yabancı";
      }
      $scope.type = name;
      $scope.typem = type;
      $scope.getList(type, 0, 20);
    };

    $scope.getList("Corporate", 0, 20);

    $scope.open = function(size, data, temp) {
      var modalInstance = $aside.open({
        templateUrl: temp,
        placement: "left",
        size: size,
        backdrop: false,
        controller: "DemoInstance2",
        resolve: {
          items: function() {
            return [data, temp, $scope.modulName];
          }
        }
      });

      modalInstance.result.then(
        function(selectedItem) {
          $scope.selected = selectedItem;
        },
        function() {
          //$log.info('Modal dismissed at: ' + new Date());
        }
      );
    };
  }
]);
