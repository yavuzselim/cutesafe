/**
 * Created by mabeh on 7.03.2018.
 */

app.controller('DemoInstance', ["$scope", "$rootScope" ,"dataProvider","toaster","$uibModalInstance",
    function($scope, $rootScope,dataProvider,toaster, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close({});
        };
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }]);

app.controller('AppCtrl', ['$rootScope', '$scope', '$state', '$swipe', '$localStorage', '$window', '$document', '$timeout', 'cfpLoadingBar',  '$transitions','dataProvider','$aside','$location',
    function ($rootScope, $scope, $state, $swipe,  $localStorage, $window, $document, $timeout, cfpLoadingBar,  $transitions,dataProvider,$aside,$location) {


        var $win = $($window), $body = $('body');

        $scope.demo = function() {


            var modalInstance = $aside.open({
                templateUrl: 'demo.html',
                placement: 'left',
                size: '',
                backdrop: true,
                controller: 'DemoInstance'

            });




            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;

            }, function() {
                //$log.info('Modal dismissed at: ' + new Date());
            });

        };

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login',''];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;

            if (restrictedPage && !angular.isDefined($localStorage.user)) {
                $location.path('/login/signin');
            }
        });


        $transitions.onStart({}, function (trans) {
            //start loading bar on stateChangeStart
            cfpLoadingBar.start();
            $scope.horizontalNavbarCollapsed = true;

            var stateTo = trans.$to();
            var name = null;
            if (stateTo.name == "app.pagelayouts.boxedpage") {
                $body.addClass("app-boxed-page");
            } else {
                $body.removeClass("app-boxed-page");
            }
            if (typeof CKEDITOR !== 'undefined') {
                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy();
                }
            }
        });

        $transitions.onSuccess({}, function (trans) {
            //stop loading bar on stateChangeSuccess
            $scope.$on('$viewContentLoaded', function (event) {
                cfpLoadingBar.complete();
            });

            // scroll top the page on change state
            $('#app .main-content').css({
                position: 'relative',
                top: 'auto'
            });

            $('footer').show();

            window.scrollTo(0, 0);

            if (angular.element('.email-reader').length) {
                angular.element('.email-reader').animate({
                    scrollTop: 0
                }, 0);
            }

            // Save the route title
            $rootScope.currTitle = $state.current.title;
        });


        $rootScope.pageTitle = function () {
            return $rootScope.config.name + ' - ' + ($rootScope.currTitle || $rootScope.config.description);
        };


    }]);