app.controller("DemoInstance", [
  "$scope",
  "$rootScope",
  "dataProvider",
  "toaster",
  "$uibModalInstance",
  "items",
  function(
    $scope,
    $rootScope,
    dataProvider,
    toaster,
    $uibModalInstance,
    items
  ) {
    var type = items[0];
    var temp = items[1];
    var modulName = items[2];
    var point = $rootScope.endPoints;
    var component = point[modulName][temp][type];
    $scope.template = component.temp;

    $scope.getRandomSpan = function() {
      var text = "";
      var possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    };
    $scope.ok = function() {
      $scope.master = $scope.myModel;

      $scope.form = {
        submit: function(form) {
          form.$setPristine(false);
          if (form.$invalid) {
            angular.forEach(form.$error.required, function(field) {
              field.$setDirty();
            });

            return;
          } else {
            if (temp == "Personal") {
              $scope.model = {
                PersonalCustomerRequest: {
                  CustomerBaseRequest: {
                    MembershipStartDate: new Date(),
                    CustomerClass: form.CustomerClass.$modelValue, //Reference
                    CancellationReason: "ReasonNotDefined",
                    PassiveReason: "PaymentNotMade",
                    LegalProceedingState: "Warning"
                  },
                  PersonalCustomerBaseRequest: {
                    Tckn: form.CustomerClass.$modelValue,
                    PersonalCustomerUpdateRequest: {
                      Name: form.Name.$modelValue,
                      Surname: form.Surname.$modelValue,
                      EducationStatus: form.EducationStatus.$modelValue,
                      Gender: form.Gender.$modelValue,
                      BirthPlace: form.BirthPlace.$modelValue,
                      BirthDate: form.BirthDate.$modelValue,
                      NationalityId: form.NationalityId.$modelValue,
                      IdentitySerialNumber:
                        form.IdentitySerialNumber.$modelValue,
                      IdentityNumber: form.IdentityNumber.$modelValue,
                      VolumeNumber: form.VolumeNumber.$modelValue,
                      FamilyNumber: form.FamilyNumber.$modelValue,
                      ItemNumber: form.ItemNumber.$modelValue
                    }
                  }
                },
                CurrentAccountRequest: {
                  CurrentAccountCode: "string",
                  CurrentAccountName: "string",
                  LastAgreementDate: "2018-03-11T01:37:00.916Z",
                  CurrentAccountType: "CustomerCurrentAccount"
                }
              };
            } else {
              $scope.model = {
                CorporateCustomerRequest: {
                  CustomerBaseRequest: {
                    MembershipStartDate: new Date(),
                    CustomerClass: form.CustomerClass.$modelValue, //Reference
                    CancellationReason: "ReasonNotDefined",
                    PassiveReason: "PaymentNotMade",
                    LegalProceedingState: "Warning"
                  },
                  CorporateCustomerBaseRequest: {
                    CorporateTaxNumber: form.CorporateTaxNumber.$modelValue,
                    CorporateCustomerUpdateRequest: {
                      CorporateTitle: form.CorporateTitle.$modelValue,
                      CorporateTaxOffice: form.CorporateTaxOffice.$modelValue,
                      CorporateShortName: form.CorporateShortName.$modelValue,
                      CorporateFoundingDate:
                        form.CorporateFoundingDate.$modelValue,
                      VatNumber: form.CorporateTaxNumber.$modelValue
                    }
                  }
                },
                CurrentAccountRequest: {
                  CurrentAccountCode: $scope.getRandomSpan(),
                  CurrentAccountName: $scope.getRandomSpan(),
                  LastAgreementDate: new Date(),
                  CurrentAccountType: "CustomerCurrentAccount"
                }
              };
            }

            dataProvider.sendJson(
              component.url,
              $scope.model,
              component.method,
              function(resp) {
                $scope.loadingBefore = false;
                var response = resp.data;

                if (response.ResponseCode == "Successfull") {
                  $scope.toaster = {
                    type: "success",
                    title: "İşlem Tamamlandı",
                    text: response.Message
                  };

                  toaster.pop(
                    $scope.toaster.type,
                    $scope.toaster.title,
                    $scope.toaster.text
                  );
                  $uibModalInstance.close(response.Result);
                } else {
                  $scope.toaster = {
                    type: "error",
                    title: "Kayıt Edilemedi",
                    text: response.Message
                  };
                  toaster.pop(
                    $scope.toaster.type,
                    $scope.toaster.title,
                    $scope.toaster.text
                  );
                }
              }
            );
          }
        },
        reset: function(form) {
          $scope.myModel = angular.copy($scope.master);
          form.$setPristine(true);
        }
      };
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss("cancel");
    };
  }
]);
app.controller("dashboardCtrl", [
  "$scope",
  "$rootScope",
  "NgTableParams",
  "dataProvider",
  "toaster",
  "$uibModal",
  "JS_REQUIRES",
  "$aside",
  function(
    $scope,
    $rootScope,
    NgTableParams,
    dataProvider,
    toaster,
    $uibModal,
    jsRequires,
    $aside
  ) {
    $scope.type = "Kurumsal";
    $scope.typem = "Corporate";

    $scope.modul_baslik = "MÜŞTERİLER";

    $scope.deleteList = function() {
      $scope.tableParams.settings().dataset = [];
      $scope.tableParams.reload();
    };
    $scope.getList = function(type, skip, take) {
      var point = $rootScope.endPoints;
      var component = point.musteriler;
      dataProvider.sendData(
        component.getList(type, skip, take),
        {},
        "GET",
        function(response, error) {
          var resp = response.data;
          if (!error) {
            var datams = resp.Result.EntityList;
            if (resp.ResponseCode == "Successfull") {
              $scope.tableParams = new NgTableParams(
                {},
                { sorting: { id: "desc" }, dataset: datams }
              );
            } else {
              $scope.deleteList();
              $scope.toaster = {
                type: "error",
                title: "Kayıt Bulunamadı",
                text: resp.Message
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          } else {
            if (resp) {
              $scope.deleteList();
              if (resp.ResponseCode == 1100) {
                $scope.toaster = {
                  type: "error",
                  title: "Kayıt Bulunamadı",
                  text: resp.Message
                };
              } else {
                $scope.toaster = {
                  type: "error",
                  title: "Müşteri Listesi Yüklenemedi",
                  text:
                    "Müşteri Listesi yüklenemedi.Lütfen Daha Sonra Tekrar Deneyin"
                };
              }

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            } else {
              $scope.toaster = {
                type: "error",
                title: "Müşteri Listesi Yüklenemedi",
                text:
                  "Müşteri Listesi yüklenemedi. Lütfen Daha Sonra Tekrar Deneyin"
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          }
        }
      );
    };
    $scope.getList2 = function(type, skip, take) {
      var point = $rootScope.endPoints;
      var component = point.musteriler;

      dataProvider.sendData(
        component.getList(type, skip, take),
        {},
        "GET",
        function(response, error) {
          var resp = response.data;
          if (!error) {
            var datams = resp.Result.EntityList;

            if (resp.ResponseCode == "Successfull") {
              $scope.loadingBefore = true;
              $scope.tableParams = new NgTableParams(
                {},
                { sorting: { id: "desc" }, data: datams }
              );
            } else {
              $scope.tableParams = {
                data: {}
              };

              $scope.toaster = {
                type: "error",
                title: "Kayıt Bulunamadı",
                text: resp.Message
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          } else {
            if (resp) {
              if (resp.ResponseCode == 1100) {
                $scope.toaster = {
                  type: "error",
                  title: "Kayıt Bulunamadı",
                  text: resp.Message
                };
              } else {
                $scope.toaster = {
                  type: "error",
                  title: "Müşteri Listesi Yüklenemedi",
                  text:
                    "Müşteri Listesi yüklenemedi.Lütfen Daha Sonra Tekrar Deneyin"
                };
              }

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            } else {
              $scope.toaster = {
                type: "error",
                title: "Müşteri Listesi Yüklenemedi",
                text:
                  "Müşteri Listesi yüklenemedi. Lütfen Daha Sonra Tekrar Deneyin"
              };

              toaster.pop(
                $scope.toaster.type,
                $scope.toaster.title,
                $scope.toaster.text
              );
            }
          }
        }
      );
    };

    $scope.changeType = function(type) {
      var name = "Kurumsal";
      if (type == "Corporate") {
        name = "Kurumsal";
      } else if (type == "Personal") {
        name = "Bireysel";
      } else if (type == "Foreigner") {
        name = "Yabancı";
      }
      $scope.type = name;
      $scope.typem = type;
      $scope.getList(type, 0, 20);
    };

    $scope.getList("Corporate", 0, 20);

    $scope.open = function(size, data, temp) {
      var modalInstance = $aside.open({
        templateUrl: temp,
        placement: "left",
        size: size,
        backdrop: false,
        controller: "DemoInstance",
        resolve: {
          items: function() {
            return [data, temp, $scope.modulName];
          }
        }
      });

      modalInstance.result.then(
        function(selectedItem) {
          $scope.selected = selectedItem;
          $scope.getList(selectedItem.CustomerType, 0, 20);
        },
        function() {
          //$log.info('Modal dismissed at: ' + new Date());
        }
      );
    };
  }
]);
