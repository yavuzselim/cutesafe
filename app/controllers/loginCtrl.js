/**
 * Created by mabeh on 2.02.2018.
 */

app.controller('login', ["$localStorage","$location","$scope","$rootScope","SweetAlert","dataProvider","toaster",
    function($localStorage,$location,$scope,$rootScope,SweetAlert,dataProvider,toaster) {



        $scope.master = $scope.myModel;

        $scope.form = {

            submit: function (form) {
                form.$setPristine(false);

                if (form.$invalid) {



                    angular.forEach(form.$error.required, function(field) {
                        field.$setDirty();
                    });



                    return;

                } else {

                    $scope.loadingBefore=true;
                    var newData = {};
                    $scope.myModel.grant_type = 'password';
                    $scope.myModel.clientcode = 'web';
                    $scope.myModel.clientpassword = '4645';

                    dataProvider.login($scope.myModel,function(resp,error){

                        if(!error){

                            var response = resp.data;

                            if(!response.error){

                                console.log(response);
                                $scope.toaster = {
                                    type: 'success',
                                    title: 'Giriş Yapıldı',
                                    text: 'Başarılı bir şekilde girişiniz onaylandı.'
                                };

                                
                                $localStorage.user = angular.copy(response);
                                toaster.pop($scope.toaster.type, $scope.toaster.title, $scope.toaster.text);
                                $location.path('/');


                            } else {
                                var msg = (JSON.parse(response.error));
                                $scope.toaster = {
                                    type: 'error',
                                    title: 'Yanlış Kullanıcı Adı ve Parola',
                                    text: 'Lütfen kullanıcı adı ve parolanızı doğru giriniz.'
                                };

                                $scope.loadingBefore = false;
                                toaster.pop($scope.toaster.type, $scope.toaster.title, $scope.toaster.text);
                            }



                        } else {


                            $scope.toaster = {
                                type: 'error',
                                title: 'Yanlış Kullanıcı Adı ve Parola',
                                text: 'Lütfen kullanıcı adı ve parolanızı doğru giriniz.'
                            };

                            $scope.loadingBefore = false;
                            toaster.pop($scope.toaster.type, $scope.toaster.title, $scope.toaster.text);

                        }


                    })



                }

            },
            reset: function (form) {


                $scope.myModel = angular.copy($scope.master);
                form.$setPristine(true);


            }
        };




    }]);
