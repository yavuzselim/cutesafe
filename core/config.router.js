/**
 * Created by mabeh on 7.03.2018.
 */

app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
    function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires) {


        app.controller = $controllerProvider.register;
        app.directive = $compileProvider.directive;
        app.filter = $filterProvider.register;
        app.factory = $provide.factory;
        app.service = $provide.service;
        app.constant = $provide.constant;
        app.value = $provide.value;


        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: jsRequires.modules
        });


        $urlRouterProvider.otherwise("/app/dashboard");

        $stateProvider.state('app', {
            url: "/app",
            templateUrl: "views/app.html",
            resolve: loadSequence('ngTable','ui.mask'),
            abstract: true
        }).state('app.dashboard', {
            url: "/dashboard",
            templateUrl: "views/components/musteriler/main.html",
            resolve: loadSequence( 'dashboardCtrl'),
            title: 'Anasayfa',
            ncyBreadcrumb: {
                label: 'Anasayfa'
            },
            controller: function ($scope) {

                $scope.modulName= 'musteriler'

                $scope.getOption = function(){
                    return 'views/components/musteriler/option.html';
                }

            }
        }).state('app.bayiler', {
            url: "/bayiler",
            templateUrl: "views/components/bayiler/main.html",
            resolve: loadSequence( 'bayilerCtrl'),
            title: 'Bayiler',
            ncyBreadcrumb: {
                label: 'Bayiler'
            },
            controller: function ($scope) {

                $scope.modulName= 'bayiler'

                $scope.getOption = function(){
                    return 'views/components/bayiler/option.html';
                }

            }
        }).state('app.logout', {
            url: '/logout',
            template: '<div ui-view class="fade-in-up"></div>',

            controller:function($localStorage){

                delete $localStorage.user;

            }
        }).state('login', {
            url: '/login',
            templateUrl: "views/login.html",
            abstract: true
        }).state('login.signin', {
            url: '/signin',
            templateUrl: "views/login_login.html",
            resolve: loadSequence( 'loginCtrl'),
            title: 'CuteSafe Giris',
            ncyBreadcrumb: {
                label: 'CuteSafe Giris'
            },
            controller: function ($scope,$localStorage,$location) {

                if (angular.isDefined($localStorage.user)) {
                    $location.path('/');
                }
            }
        })








        function loadSequence() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q',
                    function ($ocLL, $q) {
                        var promise = $q.when(1);
                        for (var i = 0, len = _args.length; i < len; i++) {
                            promise = promiseThen(_args[i]);
                        }
                        return promise;

                        function promiseThen(_arg) {
                            if (typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function () {
                                    var nowLoad = requiredData(_arg);
                                    if (!nowLoad)
                                        return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    return $ocLL.load(nowLoad);
                                });
                        }

                        function requiredData(name) {
                            if (jsRequires.modules)
                                for (var m in jsRequires.modules)
                                    if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
                                        return jsRequires.modules[m];
                            return jsRequires.scripts && jsRequires.scripts[name];
                        }
                    }]
            };
        }

    }]);