/**
 * Created by mabeh on 7.03.2018.
 */
var app = angular.module('app', ['cutesafe']);

app.run(['$localStorage','$rootScope', 'Colors','$state', '$stateParams','$http',"dataProvider","$location",
    function ($localStorage,$rootScope, Colors, $state, $stateParams,$http,dataProvider,$location) {



        $rootScope.maps = {
            musteriler:{Personal:['Id','Name','Surname','Tckn','MembershipStartDate'],Corporate:['Id','CorporateTitle','VatNumber','MembershipStartDate'],Foreigner:['Id','Name','Surname','PassportNumber','MembershipStartDate']}
        };
        $rootScope.endPoints = {
            musteriler : {

               
                add: {
                    Personal: {method:'POST',url:'/customer/createpersonalcustomer',temp:'views/components/musteriler/add_personal.html'},
                    Corporate: {method:'POST',url:'/customer/createcorporatecustomer',temp:'views/components/musteriler/add_corporate.html'},
                    Foreigner: {method:'POST',url:'/customer/createforeignercustomer',temp:'views/components/musteriler/add_foreigner.html'},
                } ,
                update: {
                    Personal :{method:'PUT',url:'/customer/{id}/update/personalcustomer'},
                    Corporate:{method:'PUT',url:'/customer/{id}/update/corporatecustomer'}
                } ,
                delete: {method:'DELETE',url:'/customer/delete/{id}'} ,
                getList:function(customerType,skip,take){
                    return '/customer/getall/customertype/'+customerType+'/skip/'+skip+'/take/'+take
                },
                getDetail : {
                    id:'/customer/get/id/{id}',
                    tckn:'/customer/get/tckn/{tckn}',
                    vat:'/customer/get/vatNumber/{vat}',
                }

            },
            bayiler : {

                add: {
                    Personal: {method:'POST',url:'/dealer/createpersonaldealer',temp:'views/components/bayiler/add-personal.html'},
                    Corporate: {method:'POST',url:'/dealer/createcorporatedealer',temp:'views/components/bayiler/add-corporate.html'},

                } ,
                update: {
                    Personal :{method:'PUT',url:'/dealer/{id}/update/personaldealer',temp:'views/components/bayiler/add-personal.html'},
                    Corporate:{method:'PUT',url:'/dealer/{id}/update/corporatedealer',temp:'views/components/bayiler/add-corporate.html'}
                } ,
                delete: {method:'DELETE',url:'/dealer/delete/{id}'} ,
                getList:function(customerType,skip,take){
                    return '/dealer/getall/skip/'+skip+'/take/'+take
                },
                getDetail : {
                    id:'/customer/get/id/{id}',

                }


            },

        }

        var config = {
            name: 'Dashboard',
            container: 'container-fluid',
            description:'',
            font: 'roboto',
            theme: 'palette-5',
            palette: Colors.getPalette('palette-5'),
            language: 'en',
            layout: 'horizontal-navigation-3',
            colors: Colors.get(),
            direction: 'ltr' //rtl
        };

        delete $localStorage.config;
        if (!angular.isDefined($localStorage.config)) {
            $localStorage.config =  angular.copy(config);
        }

        if (!angular.isDefined($localStorage.user)) {
            $location.path('/login/signin');
        }




        $('body').attr('data-layout', $localStorage.config.layout);
        $('body').attr('data-palette', $localStorage.config.theme);
        $('body').attr('data-direction', $localStorage.config.direction);






        $rootScope.authCheck = function(){

            if (!angular.isDefined($localStorage.user)) {
                /*getToken()*/
                $location.path('/login/signin');

            }
        }

      


    }]);

app.config(['cfpLoadingBarProvider',
    function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;

    }]);
app.service('dataProvider', [  "$http","$localStorage","$location",function($http,$localStorage,$location) {






    var object = {
        login:function(data,callback){

            var content_type = 'application/x-www-form-urlencoded';
            $http({
                method: 'POST',
                url: 'http://52.233.193.197:15203/token',
                data:$.param(data),
                headers: {'Content-Type': content_type,'Accept':'application/json'},
            }).then(function successCallback(response) {
                return callback(response,false);
            }, function errorCallback(response) {
                return callback(response,true);
            });
        },
        sendJson: function(url,data,method,callback){
            var token = $localStorage.user.access_token;
            //var content_type = 'application/x-www-form-urlencoded';
            var content_type ='application/json';
            var api_url = 'http://52.233.193.197:15203/api/admin';
            $http({
                method: method,
                url: api_url+url,
                data:data,
                headers: {'Content-Type': content_type,'Authorization':'bearer '+token,'Accept':content_type},
            }).then(function successCallback(response) {
                return callback(response,false);
            }, function errorCallback(response) {
                return callback(response,true);
            });

        },
        sendData: function(url,data,method,callback){
            var token = $localStorage.user.access_token;
            //var content_type = 'application/x-www-form-urlencoded';
            var content_type ='application/json';
            var api_url = 'http://52.233.193.197:15203/api/admin';
            $http({
                method: method,
                url: api_url+url,
                data:$.param(data),
                headers: {'Content-Type': content_type,'Authorization':'bearer '+token,'Accept':content_type},
            }).then(function successCallback(response) {
                return callback(response,false);
            }, function errorCallback(response) {
                return callback(response,true);
            });

        }
    }

    return object;

}]);