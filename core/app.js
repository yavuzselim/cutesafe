/**
 * Created by mabeh on 7.03.2018.
 */
'use strict';
angular.module("cutesafe",[
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'angularMoment',
    'oc.lazyLoad',
    'oitozero.ngSweetAlert',
    'toaster',
    'ngAside',
    'vAccordion',
    'vButton',
    'cfp.loadingBar',

]);