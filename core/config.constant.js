/**
 * Created by mabeh on 7.03.2018.
 */
'use strict';

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {

        //*** Controllers
        'dashboardCtrl': 'app/controllers/dashboard.js',
        'bayilerCtrl': 'app/controllers/bayiler.js',
        'loginCtrl': 'app/controllers/loginCtrl.js'



    },
    //*** angularJS Modules
    modules: [
        {
            name: 'toaster',
            files: ['bower_components/AngularJS-Toaster/toaster.js', 'bower_components/AngularJS-Toaster/toaster.css']
        },
        {
            name: 'ngAppear',
            files: ['../bower_components/angular-appear/build/angular-appear.min.js']
        },
        {
            name: 'ngTable',
            files: ['bower_components/ng-table/dist/ng-table.min.js', 'bower_components/ng-table/dist/ng-table.min.css']
        }, {
            name: 'ui.mask',
            files: ['bower_components/angular-ui-mask/dist/mask.min.js']
        }

    ]
});